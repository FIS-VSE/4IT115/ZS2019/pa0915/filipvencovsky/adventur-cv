package cz.vse.java.filipvencovsky.xnameadventuracv.main;

import cz.vse.java.filipvencovsky.xnameadventuracv.logika.Hra;
import cz.vse.java.filipvencovsky.xnameadventuracv.logika.IHra;
import cz.vse.java.filipvencovsky.xnameadventuracv.logika.PrikazJdi;
import cz.vse.java.filipvencovsky.xnameadventuracv.logika.Prostor;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Point2D;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;

import java.util.HashMap;
import java.util.Map;

public class HomeController implements Observer {

    @FXML
    public TextField vstup;
    @FXML
    public TextArea vystup;
    @FXML
    public Button odesli;
    @FXML
    public ListView seznamVychodu;
    @FXML
    public ImageView hrac;

    private IHra hra;

    private Map<String, Point2D> souradniceProstoru = new HashMap<>();

    public void incializuj(IHra hra) {
        this.hra = hra;
        vystup.setText(hra.vratUvitani()+"\n\n");
        vystup.setEditable(false);
        vstup.requestFocus();

        souradniceProstoru.put("domeček", new Point2D(14,96));
        souradniceProstoru.put("les", new Point2D(82,62));
        souradniceProstoru.put("hluboký_les", new Point2D(144,98));
        souradniceProstoru.put("chaloupka", new Point2D(203,63));
        souradniceProstoru.put("jeskyně", new Point2D(157,151));

        hra.getHerniPlan().registerObserver(this);

        update();
    }

    public void zpracujVstup(ActionEvent actionEvent) {
        zpracujPrikaz(vstup.getText());
    }

    public void zpracujPrikaz(String prikaz) {
        vystup.appendText("Příkaz: "+prikaz+"\n\n");
        vystup.appendText(hra.zpracujPrikaz(prikaz)+"\n\n");
        vstup.clear();

        if(hra.konecHry()) {
            vystup.appendText("\n"+hra.vratEpilog());
            vstup.setDisable(true);
            odesli.setDisable(true);
            seznamVychodu.setDisable(true);
        }
    }

    /**
     * Akce vyvolaná po změně předmětu pozorování
     */
    @Override
    public void update() {
        seznamVychodu.getItems().clear();
        seznamVychodu.getItems().addAll(hra.getHerniPlan().getAktualniProstor().getVychody());

        hrac.setX(souradniceProstoru.get(hra.getHerniPlan().getAktualniProstor().getNazev()).getX());
        hrac.setY(souradniceProstoru.get(hra.getHerniPlan().getAktualniProstor().getNazev()).getY());
    }

    public void jdiClick(MouseEvent mouseEvent) {
        Prostor vybranyVychod = (Prostor) seznamVychodu.getSelectionModel().getSelectedItem();
        zpracujPrikaz(PrikazJdi.getNazevStatic()+ Hra.getOddelovac()+vybranyVychod);
    }
}
