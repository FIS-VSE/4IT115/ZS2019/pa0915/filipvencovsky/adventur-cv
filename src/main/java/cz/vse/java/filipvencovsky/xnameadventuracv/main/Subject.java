package cz.vse.java.filipvencovsky.xnameadventuracv.main;

public interface Subject {

    /**
     * Upozornění pozorovatelů na změnu
     */
    void notifyObservers();

    /**
     * Registrace nového pozorovatele
     * @param observer nový pozorovatel
     */
    void registerObserver(Observer observer);

    /**
     * Odebrání stávajícího pozorovatele
     * @param observer
     */
    void unregisterObserver(Observer observer);
}
