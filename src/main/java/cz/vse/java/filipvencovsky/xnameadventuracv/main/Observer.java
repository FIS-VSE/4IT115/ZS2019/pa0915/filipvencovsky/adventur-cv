package cz.vse.java.filipvencovsky.xnameadventuracv.main;

public interface Observer {

    /**
     * Akce vyvolaná po změně předmětu pozorování
     */
    void update();
}
